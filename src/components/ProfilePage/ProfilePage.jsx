import React, { useState } from "react";
import {
  Profile,
  Grid,
  GridControlBar,
  GridControlBarItem,
  Photo,
} from "react-instagram-ui-kit";

function ProfilePage() {
  const [pics, setPic] = useState([
    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRPamUCyqYYOZwnKsSBvg3KBfBDN2Ki5zALNzrhOIrjN6SR8N7L&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRPamUCyqYYOZwnKsSBvg3KBfBDN2Ki5zALNzrhOIrjN6SR8N7L&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRPamUCyqYYOZwnKsSBvg3KBfBDN2Ki5zALNzrhOIrjN6SR8N7L&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRPamUCyqYYOZwnKsSBvg3KBfBDN2Ki5zALNzrhOIrjN6SR8N7L&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRPamUCyqYYOZwnKsSBvg3KBfBDN2Ki5zALNzrhOIrjN6SR8N7L&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRPamUCyqYYOZwnKsSBvg3KBfBDN2Ki5zALNzrhOIrjN6SR8N7L&usqp=CAU",
  ]);
  return (
    <div className="container">
      <div className="py-2"></div>
      <Profile
        bio={`If I Could Go Back In Time`}
        pictureSrc={`https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSgFR1ewrWyKD2FRgAqiGiYt2N4ulv0QlA6kOrtdzHfCeZR6A70&usqp=CAU`}
        username="Mavin Sao"
        fullname="Mavin Sao"
        followersData={[31, 3000000, 313]}
      />
      <Grid>
        <GridControlBar>
          <GridControlBarItem isActive>𐄹 Posts</GridControlBarItem>
          <GridControlBarItem>웃 Tagged</GridControlBarItem>
        </GridControlBar>
      </Grid>
      <Grid>
        {pics.map((photo) => (
          <Photo src={pics} />
        ))}
      </Grid>
      ;
    </div>
  );
}

export default ProfilePage;
