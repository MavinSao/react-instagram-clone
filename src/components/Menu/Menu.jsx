import React from "react";
import "./index.css";
import logo from "./../../assets/logo/logo.svg.png";
import { Navbar, Nav } from "react-bootstrap";
import SearchBar from "../SearchBar/SearchBar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUserCircle,
  faHeart,
  faPaperPlane,
  faHome,
} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
function Menu() {
  return (
    <div>
      <Navbar bg="light" variant="light" expand="lg" fixed="top">
        <div className="container">
          <Navbar.Brand href="#home">
            <img
              alt=""
              src={logo}
              height="35"
              className="d-inline-block align-top"
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="m-auto">
              <SearchBar />
            </Nav>
            <Nav>
              <Link to="/" className="text-dark">
                <FontAwesomeIcon icon={faHome} size="lg" className="mx-3" />
              </Link>
              <FontAwesomeIcon icon={faPaperPlane} size="lg" />
              <FontAwesomeIcon icon={faHeart} size="lg" className="mx-3" />
              <Link to="profile" className="text-dark">
                <FontAwesomeIcon icon={faUserCircle} size="lg" />
              </Link>
            </Nav>
          </Navbar.Collapse>
        </div>
      </Navbar>
      <div className="mb-5"></div>
    </div>
  );
}

export default Menu;
