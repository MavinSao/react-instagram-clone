import Item from "../Item/Item";
import React, { Component } from "react";
import Axios from "axios";

export default class Items extends Component {
  constructor() {
    super();
    this.state = { posts: [] };
  }
  componentWillMount() {
    Axios.get(
      "http://110.74.194.124:15011/v1/api/articles?page=1&limit=15"
    ).then((res) => {
      this.setState({
        posts: res.data.DATA,
      });
    });
  }
  render() {
    return (
      <div>
        {this.state.posts.map((post) => (
          <Item post={post} />
        ))}
      </div>
    );
  }
}
