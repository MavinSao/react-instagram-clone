import React, { useState } from "react";
import Users from "../Item/Users";

function UserSuggestions() {
  const [users, setUsers] = useState([1, 2, 3, 4]);
  return (
    <div className="mt-3 ml-2">
      <h4 className="my-3">Suggestions For You</h4>
      {users.map((user) => (
        <Users />
      ))}
    </div>
  );
}

export default UserSuggestions;
