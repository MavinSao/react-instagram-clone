import React, { useState } from "react";
import { Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUserCircle,
  faHeart,
  faComment,
  faShareSquare,
} from "@fortawesome/free-regular-svg-icons";
function Item({ post }) {
  const [love, setLove] = useState(false);
  return (
    <div className="card my-3">
      <Card.Header className="bg-white">
        <FontAwesomeIcon icon={faUserCircle} size="lg" /> Username
      </Card.Header>
      <Card.Img variant="top" src={post.IMAGE} />
      <Card.Body>
        <div className="my-2">
          <FontAwesomeIcon
            icon={faHeart}
            size="lg"
            className={love ? "text-danger" : "text-dark"}
            onClick={() => setLove(!love)}
          />
          <FontAwesomeIcon icon={faComment} size="lg" className="mx-3" />
          <FontAwesomeIcon icon={faShareSquare} size="lg" />
        </div>

        <Card.Title>{post.TITLE}</Card.Title>
        <Card.Text>{post.DESCRIPTION}</Card.Text>
      </Card.Body>
    </div>
  );
}

export default Item;
