import React from "react";
import { Image } from "react-bootstrap";
function Users() {
  return (
    <div className="row my-2">
      <div className="col-md-4">
        <Image
          src="https://cdn1.i-scmp.com/sites/default/files/styles/768x768/public/images/methode/2018/02/27/24b1e6ce-1b6a-11e8-804d-87987865af94_1280x720_114344.JPG?itok=e1_VQjip"
          alt="img"
          className="img-fluid"
          roundedCircle
        />
      </div>
      <div className="col-md-6">
        <b>Username</b>
        <p>Friend</p>
      </div>
      <div className="col-md-1">
        <p className="text-primary">Follow</p>
      </div>
    </div>
  );
}

export default Users;
