import React from "react";
import "./index.css";
import { Form, FormControl } from "react-bootstrap";

function SearchBar() {
  return (
    <Form>
      <FormControl
        className="text-center"
        type="text"
        placeholder="Search"
        size="sm"
      />
    </Form>
  );
}

export default SearchBar;
