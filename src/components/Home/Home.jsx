import React from "react";
import Items from "../Items/Items";
import UserSuggestions from "../Items/UserSuggestions";

function Home() {
  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col-md-9">
            <Items />
          </div>
          <div className="col-md-3">
            <UserSuggestions />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
