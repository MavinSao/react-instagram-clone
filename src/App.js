import React from 'react';
import './App.css';
import Home from './components/Home/Home';
import Menu from "./components/Menu/Menu";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import ProfilePage from './components/ProfilePage/ProfilePage';
function App() {
  return (
    <div className="App">
      <Router>
        <Menu />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/profile' component={ProfilePage} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
